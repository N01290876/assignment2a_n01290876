﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="WebProgramming.aspx.cs" Inherits="Assignment2a.About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="Web_Programming" runat="server">

   <div class="row">
        <div class="col-md-3">
            <h2>Tricky concept</h2>
            <p>The following code sorts an elements in an array alphabetically:</p>
                <p style="color:dodgerblue">var fruits = ["Kiwi", "Banana", "Apple"];</p>
                <p style="color:dodgerblue">fruits.sort();</p>
                <p><em><strong>Results: Apple,Banana,Kiwi</em></strong></p>
          
        </div>
        <div class="col-md-3">
            <h2>My snippet of code</h2>
            <p>Push method in an array adds new items to the end of an array, and returns the new length: </p> 
            <p style="color:hotpink">var fruits = ["Kiwi", "Banana", "Apple"];</p>
            <p style="color:hotpink">fruits.push("Orange");</p>
            <p><em><strong>Before push:</em></strong>Kiwi,Banana,Apple</p>
            <p><em><strong>After push:</em></strong>Kiwi,Banana,Apple,Orange</p>
      
        </div>
        <div class="col-md-3">
            <h2>Interesting code</h2>
            <p>Math.round()</p>
            <p></p>Math.round(x) returns the value of x rounded to its nearest integer:</p>
            <p  style="color:yellowgreen">Math.round(4.7); <em><strong>// returns 5</em></strong></p>    
            <p  style="color:yellowgreen">Math.round(4.4); <em><strong>// returns 4</em></strong></p> 
            <p><em>I got this code from w3school online web tutorial</em></p>

            
        </div>
        <div class="col-md-3">
            <h2>Helpful links</h2>
            <p style="color:fuchsia">https://www.w3schools.com/js/js_math.asp</p>
            <p style="color:fuchsia">https://www.w3schools.com/jsref/jsref_sort.asp</p>
            <p style="color:fuchsia">https://www.w3schools.com/jsref/jsref_push.asp</p>
           
        </div>
    </div>

</asp:Content>
