﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Database.aspx.cs" Inherits="Assignment2a.About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="Database_Design" runat="server">

    <div class="row">
        <div class="col-md-3">
            <h2>Tricky concept</h2>
            <p>
            An sql JOIN clause is used to combine data from two or more tables, based on a related column between them.
            </p>
        </div>
        <div class="col-md-3">
            <h2>My snippet of code</h2>
            <p style="color:dodgerblue">SELECT sysdate FROM DUAL</p>
            <p>The above code helps us to check the system date.</p>

            
      
        </div>
        <div class="col-md-3">
            <h2>Interesting code</h2>
            <p>
               The LIKE operator is used in a WHERE clause to search for a specified pattern in a column. 
            </p>
            <p><strong><em>Example 1:</em></strong> </p>
            <p>Finding values that starts with "a":</p>
            <p  style="color:yellowgreen">WHERE CustomerName LIKE 'a%'</p>
            <p><strong><em>Example 2: </em></strong></p>
            <p>Finding values that ends with "a":</p>
            <p  style="color:yellowgreen">WHERE CustomerName LIKE '%a'</p>
            <p><em>I got this code from w3school online web tutorial</em></p>
             
            
        </div>
        <div class="col-md-3">
            <h2>Helpful links</h2>
            <p style="color:fuchsia">https://www.w3schools.com/sql/sql_wildcards.asp </p>
            <p style="color:fuchsia">https://www.w3resource.com/sql/sql-dual-table.php </p>
            <p style="color:fuchsia">https://www.w3schools.com/sql/sql_join_inner.asp </p>

           
        </div>
    </div>

</asp:Content>
