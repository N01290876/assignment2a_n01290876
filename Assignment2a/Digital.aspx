﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Digital.aspx.cs" Inherits="Assignment2a.About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="Digital_Design" runat="server">

    <div class="row">
        <div class="col-md-3">
            <h2>Tricky concept</h2>
            <p> Margins allow us to set spacing for all edges of an element. The properties used are: </p>
            <p style="color:dodgerblue">margin-left, margin-right, margin-top, margin-bottom.</p>

            
          
        </div>
        <div class="col-md-3">
            <h2>My snippet of code</h2>
           
                 <p>List can be nested (lists inside lists):</p>
                <p style="color:hotpink"><strong><em>opening 1st unorganised list tag</em></strong></p>
                    <p><strong><em>opening list-tag</em></strong> Coffee <strong><em>closing list tag</em></strong></p>
                    <p><strong><em>opening list-tag</em></strong> Tea </p>
                        <p style="color:lightpink"><strong><em>opening 2nd unorganised list tag</em></strong></p>
                            <p><strong><em>opening list-tag</em></strong> Black tea <strong><em>closing list tag</em></strong></p>
                            <p><strong><em>opening list-tag</em></strong> Green tea <strong><em>closing list tag</em></strong></p>
                        <p style="color:lightpink"><strong><em>closing 2nd unorganised list tag</em></strong></p>
                    <p><strong><em>closing list tag</em></strong></p>
                    <p><strong><em>opening list-tag</em></strong> Milk <strong><em>closing list tag</em></strong></p>
               <p style="color:hotpink"><strong><em>closing 1st unorganised list tag</em></strong></p>
               

        
      
        </div>
        <div class="col-md-3">
            <h2>Interesting code</h2>
            <p>The :hover selector is used to select elements and style them when you mouse over them:</p>
            <p style="color:yellowgreen">a:hover {</p>
                <p style="color:yellowgreen">background-color: yellow;</p>
               <p style="color:yellowgreen">}</p>
            <p><em>I got this code from w3school online web tutorial</em></p>
            
        </div>
        <div class="col-md-3">
            <h2>Helpful links</h2>
            <p style="color: fuchsia">https://www.w3schools.com/html/html_lists.asp</p>
            <p style="color:fuchsia">https://www.w3schools.com/cssref/sel_hover.asp</p>
        </div>
    </div>

</asp:Content>

